# CGI kodutöö 2019 suvi

## Käivitamine
Käivita Docker vastavalt docker kaustas oleva README faili juhistele.
Kui serveri ip aadress või port on erinevad, kui algses dockeri server.scm, tuleks ka failis
```
grails-app/controllers/cgi/WavController
```
vastavalt muutujad 'server' ja 'port' muuta faili esimestel ridadel.


Kasutades grailsi, jooksuta rakendus
```
grails run-app
```
Rakenduse peatamine
```
grails stop-app
```
Ava brauseris localhosti aadress koos grailsi poolt kasutatava pordinumbriga(tavaliselt 8000 või 8080).
```
localhost:pordinumber/wav
```
või
```
localhost:pordinumber
```
Viimast varianti kasutades liikuda edasi valides link WavControllerile


## Kasutamine
Nupp 'Generate wave' loob tekstikastis olevast tekstist audiofaili ning salvestab selle andmebaasi.

Nupp 'Preview' esitab antud teksti audio ilma faili salvestamata.

Tabelis nupp Play mängib vastava helifaili ning nupp Download laeb faili kasutajale alla.

Kasutades nuppu 'Edit file name' muudetakse faili nime, vastavalt sisestatud tekstile nupu kõrval asuvas tekstiväljas.


## Töö kirjeldus
[Link töö kirjeldusele](https://bitbucket.org/Biilik/cgi_kodu_2019/wiki/Töö%20kirjeldus)





