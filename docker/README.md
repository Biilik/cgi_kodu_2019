# Text-to-speech teenusserver (TTS).

## Kirjeldus
Baseerib Festival projektil http://www.cstr.ed.ac.uk/projects/festival/.
Festival rakenduse Kasutusjuhend http://www.cstr.ed.ac.uk/projects/festival/manual/festival_toc.html

Lisaks on eelnevalt alla laetud Eesti Keele Instituudi (EKI) poolt loodud eestikeelne hääl Riina https://www.eki.ee/heli/index.php?option=com_content&view=article&id=6:k%C3%B5nes%C3%BCntees&catid=2&Itemid=465#yksustevalik

## Kasutamine
Käsureal (Windows 10 puhul eelistatult PowerShell) käivitada järgmised käsklused

Docker'i konteineri kokku ehitamine (eeldatud on, et käsureal ollakse antud projekti kataloogis ./text-to-speech-praktika/docker)
```
docker build -t tts .
```

Ehitatud konteineri käima panek (toimub suhtluspordi suunamine arvutist 1314 pordilt konteineri 1314 pordile)
```
docker run -p 1314:1314 tts
```


Kõigi konteinerite seiskamine
```
docker stop $(docker ps -a -q)
```


Festival serveri kohalikke seadeid võib muuta server.scm failis. Muutmise korral on vajalik uuesti konteineri kokku ehitamine.
Võib olla vajalik, muuta kui kohaliku masina IP aadressi pealt ei ole lubatud Festival serveri poole pöörduda.
Näiteks Windows 7 puhul tuleb kasutada vana Docker Toolbox'i, mille puhul on vaja kirjeldada täpne IP aadress (luuakse vahele eraldi virtuaal masin).