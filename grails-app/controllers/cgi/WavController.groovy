package cgi

import festival.FestivalClient
import sun.audio.AudioData
import sun.audio.AudioDataStream
import sun.audio.AudioPlayer

import java.nio.charset.Charset
import java.time.LocalDateTime

import static org.springframework.http.HttpStatus.*

class WavController {
    //Määrame serveri ja pordi ning loome ühenduse festivali appiga
    String server = "localhost";
    int port = 1314;


    WavService wavService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    //index leht
    def index(Integer max) {
        List<Wav> files;

        //Salvestame failide listi muutujasse wavsList. Viimasena loodud objekt salvestatakse ka muutujasse recentFile,
        // kui see eksisteerib.
        if (Wav.list().size() > 0)
            files = Wav.list(sort:'time', order: 'desc')

        if (files != null && files.size() >= 1)
            [wavsList: files, recentFile: files.get(0)]
        else
            [wavsList: Arrays.asList(), recentFile: Arrays.asList()]

    }

    //Faili salvestamine
    def save(String wav) {
        //Loome uue Wav domain objekti
        Wav w = new Wav()

        //Valime esimesed (kuni) 200 tähte kirjelduseks
        wav.length() < 200 ? (w.contentPreview = wav) : (w.contentPreview = wav.substring(0, 100))

        //Loome festivali rakenduse kaudu faili sisu ning salvestame faili
        try {
            FestivalClient fc = new FestivalClient(server, port);

            //Muudame kodeeringu UTF-8 pealt ISO-8859-15 peale, et kõik tähed oleks 1-baidised
            // festival on vana app ja ei tööta mitmebaidiste tähtedega
            //ISO...-15 on versioon, mille puhul on eesti keele tähtede kodeering lõpule viidud.(Wikipedia)
            // https://www.web3.lu/character-encoding-for-festival-tts-files/
            byte[] bytes = wav.getBytes(Charset.forName("UTF-8"));
            wav = new String(bytes, Charset.forName("ISO-8859-15"));

            //Stringi teisendamine .wav failiks
            byte[] waveBytes = fc.StringToWave(wav);

            //Lisame baidid ja faili nime(datetime) meie wav objektile ning salvestame
            w.fileBytes = waveBytes;
            w.fileName = LocalDateTime.now().toString();
            w.time = LocalDateTime.now()

            w.save()
            wavService.save(w)

            //teade audiofaili loomise õnnestumisest
            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'w.id', default: 'Wav'), w.fileName])
                }
                '*'{ respond wav, [status: OK] }
            }

            //teade ebaõnnestumisst
        } catch (Exception e) {
            flash.message = "Invalid input"
        }
        finally {
            redirect action: 'index'
        }

    }

    //Loeme baidid andmebaasist sisse ja esitame selle
    def playWav() {
        try
        {
            //Võtame objekti id järgi
            Wav el = wavService.get(Long.parseLong(params.id))

            // Loome AudioData(Stream) objekti baitide arrayst
            AudioData audiodata = new AudioData(el.fileBytes);
            AudioDataStream audioStream = new AudioDataStream(audiodata);
            // Mängime heli AudioPlayeriga
            AudioPlayer.player.start(audioStream);

        }
        catch (Exception exc)
        {
            flash.message = 'File playback error'
        }
        finally {

            redirect action: "index"
        }
    }

    def download() {


        try {
            //Võtame andmebaasist elemendi id järgi
            Wav el = wavService.get(Long.parseLong(params.id))

            //Sain abi https://stackoverflow.com/questions/6109859/grails-download-file/22679796
            // ja http://grails-dev.blogspot.com/2013/07/file-upload-functionality-2-download.html
            //Määrame tüübi, pikkuse ja headeri
            response.contentType = "application/octet-stream"
            response.contentLength = el.fileBytes.length
            response.setHeader("Content-disposition", "attachment;filename=\"${el.fileName}.wav\"")

            //Kirjutame/laeme faili alla
            response.outputStream.write(el.fileBytes)
        } catch (Exception e) {
            flash.message = 'Error while downloading file'
        }
        //Ei pea index lehele suunama kasutades ainult response'i
    }


    //Faili esitamine, ilma salvestamata
    def preview(String wav) {

        try {
            if (wav == null || wav.matches(" *"))
                throw new UnsupportedOperationException("Input must contain characters.")

            FestivalClient fc = new FestivalClient(server, port);

            //Muudame kodeeringu UTF-8 pealt ISO-8859-15 peale, et kõik tähed oleks 1-baidised
            // festival on vana app ja ei tööta mitmebaidiste tähtedega
            // https://www.web3.lu/character-encoding-for-festival-tts-files/
            byte[] bytes = wav.getBytes(Charset.forName("UTF-8"));
            wav = new String(bytes, Charset.forName("ISO-8859-15"));

            byte[] waveBytes = fc.StringToWave(wav);

            //Esitame audio
            AudioData audiodata = new AudioData(waveBytes);
            AudioDataStream audioStream = new AudioDataStream(audiodata);
            AudioPlayer.player.start(audioStream);

        } catch (Exception e) {
            flash.message = 'Preview not playable'
            println e.message
        }
        finally {
            redirect action: 'index'
        }
    }

    //Faili nime muutmine
    def edit(String newFileName) {
        try {
            //kontrollime ega siestatud sõne ei koosne ainult tühikust(tühi sõne kaasa arvatud)
            if (newFileName.matches(" *"))
                throw new UnsupportedOperationException("Input must contain characters.");
            //Saame andmebaasist objekti id kaudu, määrame uue failinime ja salvestame selle
            Wav el = wavService.get(Long.parseLong(params.id))
            el.fileName = newFileName
            el.save()
            wavService.save(el)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'el.id', default: 'Wav'), el.fileName])
                }
                '*'{ respond wav, [status: OK] }
            }

        } catch (Exception e) {
            flash.message = 'Error changing file name'
            println e.message
        }
        finally {
            redirect action: 'index'
        }
    }

}
