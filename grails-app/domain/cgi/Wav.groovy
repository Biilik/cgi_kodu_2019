package cgi

import java.time.LocalDateTime

class Wav {

    //Failinimi
    String fileName;
    //Faili baidid
    byte[] fileBytes;
    //Faili sisu näidis
    String contentPreview

    //Faili (algne) salvestamise aeg
    LocalDateTime time;

    //Lubame suuremat faili lisada
    //https://guides.grails.org/grails-upload-file/guide/index.html ,punkt 4.1
    //"	Images may be big files; we use the mapping closure to configure the sqlType" - longblobi kohta

    static constraints = {
        fileBytes column: 'file_bytes', sqlType: 'longblob'
    }
}
