<!DOCTYPE html>
<html>

    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'wav.label', default: 'Wav')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-wav" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/wav')}"><g:message code="default.home.label"/></a></li>
            </ul>
        </div>
            <g:form resource="${this.wav}" method="POST">
                <div class="row">
                <g:textArea class="form-control" name="wav" id="wav" style="width: 50%" required="true"></g:textArea>
                <g:actionSubmit class="update" action="save" value="Generate wave" />
                <g:actionSubmit class="update" action="preview" value="Preview" />
                </div>
            </g:form>

            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

            <div id="recent_file">
                <h1>Recent file</h1>
                <table class="table-hover table-bordered">
                    <thead>
                        <th>File name(time of generation)</th>
                        <th>Content preview</th>
                    </thead>
                    <tbody>
                        <tr>
                            <g:each in="${recentFile}" var="recent">
                                <td>
                                    ${recent.fileName}
                                </td>
                                <td>
                                    ${recent.contentPreview}
                                </td>
                                <td>
                                    <g:link controller="wav" action="playWav" params="[value: 'recentFile.value']" id="${recentFile.id}">
                                        <button id="${recent.id.toString()}_play" class="btn btn-primary">Play</button>
                                    </g:link>
                                </td>
                                <td>
                                    <g:link controller="wav" action="download" params="[value: 'recentFile.value']" id="${recentFile.id}">
                                        <button class="btn btn-primary">Download</button>
                                    </g:link>
                                </td>
                                <td>
                                    <g:form resource="${this.wav}" method="POST" id="${recentFile.id}">
                                        <div class="row">
                                            <g:textField class="form-control" name="newFileName" id="newFileName" required="true" style="width: auto"></g:textField>
                                            <g:actionSubmit class="btn btn-primary" action="edit" value="Edit file name" />
                                        </div>
                                    </g:form>
                                </td>
                            </g:each>
                        </tr>
                    </tbody>
                </table>
            </div>

%{--            <f:table collection="${wavList}" />--}%
            <table class="table-hover table-bordered">
                <h1>All .wav files</h1>
                <input class="form-control" id="filter" type="text" placeholder="Search..">

                <thead>
                    <th scope="col">File name</th>
                    <th scope="col">Content preview</th>
                </thead>
                <tbody id="wav_table">
                    <g:each in="${wavsList}" var="p">
                            <tr>
                                <td>${p.fileName}</td>
                                <td>${p.contentPreview}</td>

                                <td>
                                    <g:link controller="wav" action="playWav" params="[value: 'p.value']" id="${p.id}">
                                        <button id="${p.id.toString()}_play" class="btn btn-primary">Play</button>
                                    </g:link>
                                </td>
                                <td>
                                    <g:link controller="wav" action="download" params="[value: 'p.value']" id="${p.id}">
                                        <button class="btn btn-primary">Download</button>
                                    </g:link>
                                </td>
                                <td>
                                    <g:form resource="${this.wav}" method="POST" id="${p.id}">
                                        <div class="row">
                                            <g:textField class="form-control" name="newFileName" id="newFileName" required="true" style="width: auto"></g:textField>
                                            <g:actionSubmit class="btn btn-primary" action="edit" value="Edit file name" />
                                        </div>
                                    </g:form>
                                </td>
                            </tr>
                    </g:each>
                </tbody>
            </table>


            <div class="pagination">
                <g:paginate total="${wavCount ?: 0}" />
            </div>
        </div>
    </body>
</html>