package cgi

import grails.gorm.services.Service

@Service(Wav)
interface WavService {

    Wav get(Serializable id)

    List<Wav> list(Map args)

    Long count()

    void delete(Serializable id)

    Wav save(Wav wav)

}